<?php

namespace App\Http\Controllers;

use App\Platform;
use Illuminate\Http\Request;

class PlatformController extends Controller
{
    public function index(Request $request)
    {
        if ($request->user_count == 'true') {
            $platforms = Platform::withCount('users')->get();
        } else if ($request->data == 'name') {
            $platforms = Platform::select('id', 'name')->get()->toArray();
        } else {
            $platforms = Platform::all();
        }
        
        return $platforms;
    }

    public function show(Platform $platform)
    {
        return $platform;
    }

    public function store(Request $request)
    {
        $platform = Platform::create($request->all());

        return response()->json($platform, 201);
    }

    public function update(Request $request, Platform $platform)
    {
        $platform->update($request->all());

        return response()->json($platform, 200);
    }

    public function delete(Platform $platform)
    {
        $platform->delete();

        return response()->json(null, 204);
    }

    public function generateKey()
    {
        $keys = Platform::get()->pluck('key')->toArray();
        $valid_key = false;
        while ($valid_key == false) {
            if (!function_exists('openssl_random_pseudo_bytes')) {
                throw new RuntimeException('OpenSSL extension is required.');
            }
            $bytes = openssl_random_pseudo_bytes(15 * 2);
            if ($bytes === false) {
                throw new RuntimeException('Unable to generate random string.');
            }
            $key = substr(str_replace(array('/', '+', '='), '', base64_encode($bytes)), 0, 15);
            if (!in_array($key, $keys)) {
                $valid_key = true;
            }
        }

        return response()->json([
            'key' => $key
        ], 200);
    }
}
