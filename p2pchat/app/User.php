<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Carbon\Carbon;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password', 'mobile_number', 'role', 'platform_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The attributes that should be dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    public function getCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('d-M-Y H:i:s');
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('d-M-Y H:i:s');
    }

    /**
     * Platform can have many users.
     * User can only belong to one platform. 
     *
     * @var array
     */
    public function platform()
    {
        return $this->belongsTo('App\Platform');
    }

    /**
     * User can have many chat sessions.
     * Chat session can only belong to one user. 
     *
     * @var array
     */
    public function chatSessions1()
    {
        return $this->hasMany('App\ChatSession', 'user1_id');
    }

    public function chatSessions2()
    {
        return $this->hasMany('App\ChatSession', 'user2_id');
    }

    /**
     * User can have many messages.
     * Message can only belong to one user. 
     *
     * @var array
     */
    public function messages()
    {
        return $this->hasMany('App\Messages');
    }
}
