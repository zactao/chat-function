require("./bootstrap");
import Vue from "vue";
import VueRouter from "vue-router";
Vue.use(VueRouter);
import ElementUI from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
import App from "./views/App";
import Overview from "./views/Overview";
import Chatview from "./views/Chatview";
import Login from "./views/Login";
import locale from "element-ui/lib/locale/lang/en";
import "../assets/tailwind.css";

Vue.config.productionTip = false;

const router = new VueRouter({
    mode: "history",
    routes: [
        {
            path: "/overview",
            name: "overview",
            component: Overview
        },
        {
            path: "/chatview",
            name: "chatview",
            component: Chatview
        },
        {
            path: "/login",
            name: "login",
            component: Login
        }
    ]
});

Vue.use(ElementUI, { locale });
Vue.use(require('vue-moment'));
const app = new Vue({
    el: "#app",
    components: { App },
    router
});
