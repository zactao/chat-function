<?php

use App\ChatSession;
use Illuminate\Support\Facades\Broadcast;

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

// Broadcast::channel('App.User.{id}', function ($user, $id) {
//     return (int)$user->id === (int)$id;
// });

// Broadcast::channel('chat', function ($user) {
//     return true;
// });

// Broadcast::channel('chat_session.{chatSession}', function (ChatSession $chatSession) {
//     // return $group->hasUser($user->id);
//     return true;
// });

// Broadcast::channel('public', function() {
//     return true;
// });