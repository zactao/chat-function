<?php

namespace App\Http\Controllers;

use App\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Pusher\Pusher;
use App\Events\MessageSent;
use App\User;

class MessageController extends Controller
{
    // //

    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    public function index(Request $request)
    {
        if ($request->chat_session_id) {
            $messages = Message::where('chat_session_id', '=', $request->chat_session_id)->get();

            if ($request->read == true) {
                $this->readMessages($request);
            }
        } else {
            $messages = Message::all();
        }
        return $messages;
    }

    public function show(Message $message)
    {
        return $message;
    }

    public function store(Request $request)
    {
        $message = Message::create($request->all());

        return response()->json($message, 201);
    }

    public function update(Request $request, Message $message)
    {
        // $message->update($request->all());
        $message->read = 1;
        $message->save();
        return response()->json($message, 200);
    }

    public function delete(Message $message)
    {
        $message->delete();

        return response()->json(null, 204);
    }

    public function sendMessage(Request $request)
    {
        $message = Message::create($request->all());
        $message = Message::where('id', '=', $message->id)->with('user')->first();
        event(new MessageSent($message));

        return response()->json($message, 200);
    }

    public function readMessages(Request $request)
    {
        if ($request->chat_session_id == true) {
            $current_user = $request->user();
            $messages = Message::where('chat_session_id', '=', $request->chat_session_id)->where('user_id', '!=', $current_user->id)->get();
            foreach ($messages as $message) {
                if ($message->user_id != $current_user->id) {
                    $message->read = 1;
                    $message->save();
                }
            }
        }
    }
}
