<?php

use Illuminate\Database\Seeder;
use App\Platform;

class PlatformsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Platform::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $faker = \Faker\Factory::create();

        for ($i = 0; $i < 5; $i++) {
            Platform::create([
                'name' => $faker->name,
                'description' => $faker->realText($faker->numberBetween(10, 70)),
                'key' => $faker->regexify('[A-Z0-9]{12}'),
                'publish' => false
            ]);
        }
    }
}
