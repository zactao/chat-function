<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Platform;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        User::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $faker = \Faker\Factory::create();

        $platformIds = Platform::all()->pluck('id')->toArray();

        for ($i = 0; $i < 50; $i++) {
            User::create([
                'username' => $faker->userName,
                'password' => bcrypt(' '),
                'mobile_number' => $faker->randomNumber(8),
                'platform_id' => $faker->randomElement($platformIds),
                'role' => 'member'
            ]);
        }

        User::create([
            'username' => 'admin',
            'password' => bcrypt('password'),
            'role' => 'admin'
        ]);
    }
}
