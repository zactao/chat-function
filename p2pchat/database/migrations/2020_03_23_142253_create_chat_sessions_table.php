<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChatSessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chat_sessions', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('user1_id')->unsigned()->index()->nullable();
            $table->foreign('user1_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->integer('user2_id')->unsigned()->index()->nullable();
            $table->foreign('user2_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chat_sessions');
    }
}
