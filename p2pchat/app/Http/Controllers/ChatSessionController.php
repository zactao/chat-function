<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ChatSession;
use App\Message;
use App\User;

class ChatSessionController extends Controller
{
    public function index(Request $request)
    {
        if ($request->user_id) {
            $chatSessions = [];
            $chatSessions1 = ChatSession::where('user1_id', '=', $request->user_id)->get();
            $chatSessions2 = ChatSession::where('user2_id', '=', $request->user_id)->get();

            foreach ($chatSessions1 as $chatSession) {
                $chatSession->other_user = User::where('id', '=', $chatSession->user2_id)->first();
                $messages = Message::where('chat_session_id', '=', $chatSession->id)->get();
                $read_count = 0;
                foreach ($messages as $message) {
                    if ($message->user_id != $chatSession->user1_id) {
                        if ($message->read == 0) {
                            $read_count += 1;
                        }
                    }
                }
                $chatSession->read_count = $read_count;
                $chatSession->latest_message = $messages->last();

                array_push($chatSessions, $chatSession);
            }

            foreach ($chatSessions2 as $chatSession) {
                $chatSession->other_user = User::where('id', '=', $chatSession->user1_id)->first();
                $messages = Message::where('chat_session_id', '=', $chatSession->id)->get();
                $read_count = 0;
                foreach ($messages as $message) {
                    if ($message->user_id != $chatSession->user2_id) {
                        if ($message->read == 0) {
                            $read_count += 1;
                        }
                    }
                }
                $chatSession->read_count = $read_count;
                $chatSession->latest_message = $messages->last();

                array_push($chatSessions, $chatSession);
            }

            // $chatSessions = $chatSessions -> 
        } else {
            $chatSessions = ChatSession::all();
        }
        return $chatSessions;
    }

    public function show(ChatSession $ChatSession)
    {
        return $ChatSession;
    }

    public function store(Request $request)
    {
        $ChatSession = ChatSession::create($request->all());

        return response()->json($ChatSession, 201);
    }

    public function update(Request $request, ChatSession $ChatSession)
    {
        $ChatSession->update($request->all());

        return response()->json($ChatSession, 200);
    }

    public function delete(ChatSession $ChatSession)
    {
        $ChatSession->delete();

        return response()->json(null, 204);
    }
}
