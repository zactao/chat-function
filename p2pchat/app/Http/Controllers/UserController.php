<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\ChatSession;

class UserController extends Controller
{
    public function index(Request $request)
    {
        if ($request->role) {
            if ($request->role == "member") {
                $users = User::where('role', '=', $request->role)->with('platform:id,name')->get();
            } else {
                $users = User::where('role', '=', $request->role)->get();
            }
        } else {
            $users = User::all();
        }
        return $users;
    }
    
    public function show(User $user, Request $request)
    {
        // echo $request->data;
        if ($request->data == "chatsessions") {
            $chatSessions = ChatSession::where('user1_id', '=', $user->id)->orWhere('user2_id', '=', $user->id)
                ->get();
            foreach ($chatSessions as $chatSession) {
                echo $chatSession;
                
            }
            return $chatSessions;
        } else {
            return $user;
        }
    }

    public function store(Request $request)
    {
        if ($request->role == 'admin') {
            $usernames = User::where('role', '=', $request->role)->get()->pluck('username')->toArray();
            if (in_array($request->username, $usernames)) {
                return response()->json([
                    'message' => 'Admin username exist!'
                ], 500);
            }
            $password = bcrypt($request->password);
            $request['password'] = $password;
        }

        $user = User::create($request->all());

        if ($request->role == 'member') {
            $user = User::where('id', '=', $user->id)->with('platform:id,name')->first();
        }

        return response()->json($user, 201);
    }

    public function update(Request $request, User $user)
    {
        if ($request->password) {
            $password = bcrypt($request->password);
            $request['password'] = $password;
        }
        $user->update($request->all());
        $user = User::where('id', '=', $user->id)->with('platform:id,name')->first();

        return response()->json($user, 200);
    }

    public function delete(User $user)
    {
        $user->delete();

        return response()->json(null, 204);
    }
}
