<?php

use function GuzzleHttp\json_encode;
// use Illuminate\Routing\Route;

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('member-mobile-login', 'AuthController@mobileNumberLogin');
    Route::post('admin-login', 'AuthController@adminLogin');
    Route::post('signup', 'AuthController@signup');
});
// display empty for now
Route::post('login', ['as' => 'login', 'uses' => 'AuthController@mobileNumberLogin']);

Route::group([
    'middleware' => 'auth:api'
], function () {
    Route::get('logout', 'AuthController@logout');
    Route::get('user', 'AuthController@user');

    Route::group([
        'middleware' => 'App\Http\Middleware\AdminMiddleware'
    ], function () {
        Route::get('platforms/generate-key', 'PlatformController@generateKey');
        Route::get('platforms', 'PlatformController@index');
        Route::get('platforms/{platform}', 'PlatformController@show');
        Route::post('platforms', 'PlatformController@store');
        Route::put('platforms/{platform}', 'PlatformController@update');
        Route::delete('platforms/{platform}', 'PlatformController@delete');

        Route::get('users', 'UserController@index');
        Route::get('users/{user}', 'UserController@show');
        Route::post('users', 'UserController@store');
        Route::put('users/{user}', 'UserController@update');
        Route::delete('users/{user}', 'UserController@delete');
    });

    Route::get('messages', 'MessageController@index');
    Route::get('messages/{message}', 'MessageController@show');
    Route::post('messages', 'MessageController@sendMessage');
    Route::put('messages/{message}', 'MessageController@update');
    Route::delete('messages/{message}', 'MessageController@delete');


    Route::get('chat_sessions', 'ChatSessionController@index');
    Route::get('chat_sessions/{chat_session}', 'ChatSessionController@show');
    Route::post('chat_sessions', 'ChatSessionController@store');
    Route::put('chat_sessions/{chat_session}', 'ChatSessionController@update');
    Route::delete('chat_sessions/{chat_session}', 'ChatSessionController@delete');
});
