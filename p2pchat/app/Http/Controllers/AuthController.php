<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Platform;
use App\User;

class AuthController extends Controller
{
    public function signup(Request $request)
    {
        $request->validate([
            'username' => 'required|string',
            'mobile_number' => 'required|string',
            'key' => 'required|string'
        ]);
        $platform = Platform::where('key', '=', $request->key)->first();
        if ($platform == null)
            return response()->json([
                'message' => 'User not created due to invalid values!'
            ], 500);

        $platform->users()->create([
            'username' => $request->username,
            'mobile_number' => $request->mobile_number,
            'password' =>  bcrypt(' '),
            'role' => 'member'
        ]);
        return $this->mobileNumberLogin($request);
    }

    public function mobileNumberLogin(Request $request)
    {
        $request->validate([
            'username' => 'required|string',
            'mobile_number' => 'required|string',
            'key' => 'required|string',
            'remember_me' => 'boolean',
        ]);
        $request->password = ' ';
        // Check if user foreign key.
        $platform = Platform::where('key', '=', $request->key)->first();
        if ($platform == null) {
            return response()->json([
                'message' => 'User not created due to invalid values!'
            ], 500);
        }
        $check = [['platform_id', '=', $platform->id], ['mobile_number', '=', $request->mobile_number], ['role', '=', 'member']];
        $user = User::where($check)->first();
        if ($user == null) {
            // If user does not exist, go to signup.
            return $this->signup($request);
        }

        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();

        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString(),
            'chatview_url' => request()->getHttpHost() . '/chatview?token=' . $tokenResult->accessToken

        ]);
    }

    public function adminLogin(Request $request)
    {
        $request->validate([
            'username' => 'required|string',
            'password' => 'required|string'
        ]);
        $credentials = request(['username', 'password']);
        if (!Auth::attempt($credentials))
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);

        $check = [['username', '=', $request->username], ['role', '=', 'admin']];
        $user = User::where($check)->first();

        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();

        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ]);
    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }

    /**
     * Get the authenticated User
     *
     * @return [json] user object
     */
    public function user(Request $request)
    {
        return response()->json($request->user());
    }
}
